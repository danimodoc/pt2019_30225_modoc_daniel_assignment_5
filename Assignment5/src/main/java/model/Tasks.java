package model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Tasks {
	
	static List<MonitoredData> obList = new ArrayList<>();
	
	public static MonitoredData parseToData(String line){
		String newS[] = line.split("\t\t");
		
		LocalDateTime sTime = LocalDateTime.parse(newS[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		String s = String.valueOf(sTime).replaceAll("T", " ");
		
		LocalDateTime eTime = LocalDateTime.parse(newS[1], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	    
	    MonitoredData obj = new MonitoredData(sTime,eTime,newS[2]);
		
		return obj;
	}
	
	public static void task1(){//Fetch data to list
		
		System.out.println("Task 1\n");
		
		String fileName = "Activities.txt";
		List<String> list = new ArrayList<>();

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			
			list = stream
					.collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(String s: list) {
			MonitoredData ob = parseToData(s);
			obList.add(ob);
		}
		
		obList.forEach(System.out::println);
	}
	
	public static void task2() {//Count distinct days
		
		System.out.println("Task 2\n");
		Long count = obList.stream()
				.map(ob->ob.getStartTime().toLocalDate()).distinct()
				.collect(Collectors.counting());
		
		System.out.println(count);
		
	}
	
	public static void task3() {
		
		System.out.println("Task 3\n");
		
		Map<Object, Long> list = new HashMap<Object,Long>();
		list = obList.stream()
				     .collect(Collectors.groupingBy(ob->ob.getActivity(), Collectors.counting()));
		
		Map<String,Integer> occurenceList = new HashMap<String,Integer>();
		
		for(Object ob : list.keySet()) {
			occurenceList.put(ob.toString().replaceAll("\t", ""),Math.toIntExact(list.get(ob)));
		}
		
		occurenceList.entrySet().stream().forEach(ob -> System.out.println(ob));
	}
	
	public static void task4() {
		System.out.println("Task 4\n");

		Map<Object, Map<Object, Long>> days = new HashMap<Object,Map<Object,Long>>();
		
		days = obList.stream()
                     .collect(Collectors
                                        .groupingBy(ob->ob.getStartTime().toLocalDate(),Collectors.groupingBy(ob->ob.getActivity(),Collectors.counting())));
		
		days.entrySet().stream().forEach(ob -> System.out.println(ob));
		
	}
	
	public static void task5() {
		System.out.println("Task 5\n");

		Map<String,Duration> durList = new HashMap<String,Duration>();
		int i=0;
		
		for(MonitoredData m: obList) {
			Duration duration = Duration.between(m.getStartTime(), m.getEndTime());
			i++;
			durList.put("Line "+i+" : ", duration);
			
			System.out.println("Line "+i+" : "+ duration);
		}
		
	}
	
	public static void task6() {
		
		System.out.println("Task 6\n");
		
		Map<Object, Duration> totalActiv = new HashMap<Object,Duration>();
		
		Duration zero = Duration.ZERO;
		
		totalActiv = obList.stream()
                           .collect(Collectors.groupingBy(ob->ob.getActivity(),// mapping pentru modificari
                                    Collectors.mapping(ob->Duration.between(ob.getStartTime(),ob.getEndTime()),
                                    Collectors.reducing(zero,(time1,time2) -> time1.plus(time2)))));// zero e primul argument al reducing
		
		totalActiv.entrySet().stream().forEach(ob -> System.out.println(ob));
	}
	
	public static void task7() {
		System.out.println("Task 7\n");
		
		Map<Object, Long> list90 = new HashMap<Object,Long>();
		Duration time = Duration.ofMinutes(5);
		
		list90 = obList.stream()
			       .filter(ob -> Duration.between(ob.getStartTime(), ob.getEndTime()).compareTo(time) < 0)//
			       .collect(Collectors.groupingBy(ob -> ob.getActivity(),Collectors.counting()));
		
		List<Object> finalS = new ArrayList<Object>();
		
		Map<String ,Long> listAll = obList.stream()
				                          .collect(Collectors.groupingBy(ob -> ob.getActivity() , Collectors.counting()));
		
		finalS =  list90.entrySet().stream()
				        .filter(e->e.getValue()/listAll.get(e.getKey())>=0.9)
				        .map(ob -> ob.getKey())
				        .collect(Collectors.toList());
		
		finalS.forEach(System.out::println);
		
	}
}
